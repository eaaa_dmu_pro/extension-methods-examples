﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ext;

namespace extensionmethods
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = "Once Upon a Time in the West";

            Console.WriteLine(s);
            Console.WriteLine();

            int n = s.WordCount();
            Console.WriteLine("Number of Words: "+n);

            string init = s.Initials();
            Console.WriteLine("Initials: "+init);

            int m=3;
            try
            {
                Console.WriteLine(m + ": " + s.ExtractWord(m));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }
    }
}
